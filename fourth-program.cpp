#include <string>
#include <vector>
#include <iostream>

class Fullname {
public:
    std::string firstname;
    std::string secondname;
};

int main() {

    Fullname name;
    std::vector<Fullname> names;
    int totalnames = 0;
    
    std::cout << "Enter the number of names you would like to input:" << std::endl;
    std::cin >> totalnames;

    int namesinputted = 0;
    while (namesinputted < totalnames) {
        
        std::cout << "Please enter your first name:" << std::endl;
        std::cin >> name.firstname;

        if (name.firstname == "Shrek") {
            std::cout << "It's Shrek!" << std::endl;
        }else {
            std::cout << "It's not Shrek!" << std::endl;
        }
    
        std::cout << "Please enter your second name:" << std::endl;
        std::cin >> name.secondname;
        
        names.push_back(name);
        namesinputted++;
    }
    
    for (int count = 0; count < names.size(); count++) {
        
        std::cout << "Hello, " << names[count].firstname << " " << names[count].secondname << std::endl;
    }
    
    return 0;
}
