#include <string>
#include <vector>
#include <iostream>

// Create the game noughts and crosses:
// At the beginning of the game the user should be prompted to select a team, either O's or X's.
// The user should be prompted to input a number that corresponds to a position on the 3x3 noughts and crosses board.
// After submitting this input, a O or a X will be placed at this position.

int choose_teams() {

    int userteam = 0;
    std::cout << "Let's play a game. Would you like to go first? [Y/N]" << std::endl;
    
    while (userteam == 0) {
        std::string response;
        std::cin >> response;
        if (response == "Y" || response == "y" || response == "Yes" || response == "yes") {
            std::cout << "Okay, you will play Xs and I will play Os." << std::endl;
            userteam = 1;
        }else if (response == "N" || response == "n" || response == "No" || response == "no") {
            std::cout << "Okay, I will play Xs and you will play Os." << std::endl;
            userteam = 2;
        }else {
        std::cout << "Let's try that again. Would you like to go first? [Y/N]" << std::endl;    
        }
    }    
    return userteam;
}

int print_board(std::vector<std::string> board) {

    std::cout << "  | A | B | C |" << std::endl;
    std::cout << "1 | " << board[0] << " | " << board[1] << " | " << board[2] << " |" << std::endl;
    std::cout << "2 | " << board[3] << " | " << board[4] << " | " << board[5] << " |" << std::endl;
    std::cout << "3 | " << board[6] << " | " << board[7] << " | " << board[8] << " |" << std::endl;
}

int get_user_input(std::vector<std::string> board) {

    int position_index;
    int no_valid_input = 1;
    
    while (no_valid_input == 1) {
        std::cout << "Please enter your move, using format A1/B2/C3 etc." << std::endl;
        std::string position;
        std::cin >> position;

        int column = 0;
        if (position[0] == 'A' || position[0] == 'a') {
            column = 1;
        }else if (position[0] == 'B' || position[0] == 'b') {
            column = 2;
        }else if (position[0] == 'C' || position[0] == 'c') {
            column = 3;
        }

        int row = 0;
        if (position[1] == '1') {
            row = 1;
        }else if (position[1] == '2') {
            row = 2;
        }else if (position[1] == '3') {
            row = 3;
        }

        if (column == 0 || row == 0) {
            std::cout << "Invalid format! ";
            continue;
        }else {
            position_index = 1*(column-1) + 3*(row-1);
            if (board[position_index] == "X" || board[position_index] == "O"){
            std::cout << "That space is occupied! ";
            continue;
            }else {
                no_valid_input = 0;
            }
        }
    
    return position_index;
    }
}

int get_cpu_input(std::vector<std::string> board) {

    int position_index;
    int no_valid_input = 1;
    std::vector<int> position_list = {4, 0, 8, 2, 6, 1, 3, 5, 7};

    while (no_valid_input == 1) {
        for (int count = 0; count < position_list.size(); count++) {
            position_index = position_list[count];
            if (board[position_index] == "X" || board[position_index] == "O"){
                continue;
            }else {
                no_valid_input = 0;
                break;
            }
        }
    return position_index;
    }
}

std::vector<std::string> initialise_board() {
    std::vector<std::string> board = { " ", " ", " ", " ", " ", " ", " ", " ", " " };
    return board;
}
        
std::vector<std::string> update_board(std::vector<std::string> board, int team,  int position_index) {

    std::string marker;
    if (team == 1) {
        marker = "X";
    }else if (team == 2) {
        marker = "O";
    }

    board[position_index] = marker;

    return board;
}

int check_board(std::vector<std::string> board, int who_moved_last){
    int gamestate = 0;
    
    if (
    // Horizontals.
    (board[0] == board[1] && board[1] == board [2] && board[2] != " ") ||
    (board[3] == board[4] && board[3] == board [5] && board[5] != " ") ||
    (board[6] == board[7] && board[7] == board [8] && board[8] != " ") ||
    // Verticals.
    (board[0] == board[3] && board[3] == board [6] && board[6] != " ") ||
    (board[1] == board[4] && board[4] == board [7] && board[7] != " ") ||
    (board[2] == board[5] && board[5] == board [8] && board[8] != " ") ||
    // Diagonals.
    (board[0] == board[4] && board[4] == board [8] && board[8] != " ") ||
    (board[6] == board[4] && board[4] == board [2] && board[2] != " ")
    ) {gamestate = who_moved_last;}

    return gamestate;
}

int main() {

    // Initialise.    
    int gamestate = 0;
    std::vector<std::string> board = initialise_board();
    int position_index;
    int userteam = choose_teams();
    int cputeam;
    if (userteam == 1) {
        cputeam = 2;
    }else if (userteam == 2) {
        cputeam = 1;
        position_index = get_cpu_input(board);
        board = update_board(board, cputeam, position_index);
    }

    print_board(board);

    // Game loop.
    while (gamestate == 0) {

        // User turn.
        position_index = get_user_input(board);
        board = update_board(board, userteam, position_index);
        print_board(board);
        gamestate = check_board(board, userteam);
        if (gamestate != 0) {break;}

        // CPU turn.
        position_index = get_cpu_input(board);
        board = update_board(board, cputeam, position_index);
        print_board(board);
        gamestate = check_board(board, cputeam);
        if (gamestate != 0) {break;}
    }

    // Game over screen.
    if (gamestate == userteam) {
        std::cout << "Congratulations! You win!" << std::endl;
    }else if (gamestate == cputeam) {
        std::cout << "You lose! Better luck next time." << std::endl;
    }

    return 0;
}
