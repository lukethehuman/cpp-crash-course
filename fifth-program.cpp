#include <iostream>

// Add two integers together. However, the addition must be done by dereferencing
// two pointers that point respectively to those two integers.
// Print the result to the screen.


int sum_and_dereference(int a, int b) {

    int result = a + b;
    int* pointerToA; int* pointerToB;
    pointerToA = &a; pointerToB = &b;
    *pointerToA = 0; pointerToB = 0;

    return result;
}

int main() {

    int a; int b;

    std::cin >> a;
    std::cin >> b;

    int output = sum_and_dereference(a, b);
    std::cout << output << std::endl;

    return 0;
}
