#include <string>
#include <vector>
#include <iostream>

// Use the above program, but don't check for the name Shrek.
// Instead, check if inputted names contain an invalid character "a" (lower and upper case).
// If a first or last name contains this invalid character then do not add it to the
// names vector, add it to an invalid names vector instead.
// Print all of the valid names and then all of the invalid names, telling the user
// which is which.

class Fullname {
public:
    std::string firstname;
    std::string secondname;
};

int Checkname(std::string teststring) {
    
    int output = 1;

    for (int count = 0; count < teststring.length(); count++) {
        if (teststring[count] == 'a' || teststring[count] == 'A') {
            output = 0;
        }
    }

    return output;
}

int main() {

    Fullname name;
    std::vector<Fullname> validnames;
    std::vector<Fullname> invalidnames;
    int totalnames = 0;

    std::cout << "Enter the number of names you would like to input:" << std::endl;
    std::cin >> totalnames;

    int namesinputted = 0;
    while (namesinputted < totalnames) {
        
        std::cout << "Please enter your first name:" << std::endl;
        std::cin >> name.firstname;

        std::cout << "Please enter your second name:" << std::endl;
        std::cin >> name.secondname;

        if (Checkname(name.firstname) && Checkname(name.secondname)) {
            std::cout << "Name valid, added to the list." << std::endl;
            validnames.push_back(name);
        }else {
            std::cout << "Name invalid, added to the naughty ist!" << std::endl;
            invalidnames.push_back(name);
        }
        
        namesinputted++;
    }
    
    for (int count = 0; count < validnames.size(); count++) {
        
        std::cout << "Hello, " 
        << validnames[count].firstname << " " 
        << validnames[count].secondname << std::endl;
    }

    for (int count = 0; count < invalidnames.size(); count++) {
        
        std::cout << "Goodbye, " 
        << invalidnames[count].firstname << " " 
        << invalidnames[count].secondname << std::endl;
    }
    
    return 0;
}
