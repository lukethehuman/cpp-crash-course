#include <iostream>
#include <string>

int main() {
    
    std::string firstname;
    std::string secondname;
    
    std::cout << "Please enter your first name:" << std::endl;
    std::cin >> firstname;
    std::cout << "Please enter your second name:" << std::endl;
    std::cin >> secondname;
    std::cout << "Hello, " << firstname << " " << secondname << std::endl;
    
    return 0;
}