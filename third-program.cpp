#include <string>
#include <iostream>
#include <vector>

int main() {
    
    std::string firstname;
    std::string secondname;
    
    std::vector<std::string> firstnames;
    std::vector<std::string> secondnames;

    int namesinputted = 0;
    while (namesinputted < 10) {
        
        std::cout << "Please enter your first name:" << std::endl;
        std::cin >> firstname;
        firstnames.push_back(firstname);
        
        std::cout << "Please enter your second name:" << std::endl;
        std::cin >> secondname;
        secondnames.push_back(secondname);

        namesinputted++;
    }
    
    for (int count = 0; count < firstnames.size(); count++) {

        std::cout << "Hello, " << firstnames[count] << " " << secondnames[count]
        << std::endl;
    }

    return 0;
}
