#include <iostream>
#include <string>

//Get the sub string, "this" from the str variable.
//Change the third and fourth character of this substring so it becomes "theo".
//Append this sub string to the original string variable, str
//Output this resulting string to the screen.


int main() {
    
    std::string str = "Hello this is a string called: ";    
    std::string substring = str.substr(6, 4);
    std::string newstring = substring.substr(0,2) + "eo";
    std::cout << str+newstring << std::endl;
    
    return 0;
}
