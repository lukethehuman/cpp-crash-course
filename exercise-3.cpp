#include <string>
#include <iostream>
#include <vector>

// Create a program which calculates the average of a user inputted list of
// integer values.
// First, prompt the user to enter the amount of numbers they would like to add.
// Then prompt the user to add this many integer numbers.
// Calculate the average value of the integer numbers
// Output this result to the screen.

int main() {
 
    std::cout << "Hello, I will take some numbers and give you the average."
        << std::endl;
    std::cout << "How many numbers would you like to add?"
        << std::endl;
    int N; std::cin >> N;

    int number;
    std::vector<int> numberlist;
    for (int count = 0; count < N; count++) {
        
        std::cout << "Please enter a number..."
            << std::endl;
        std::cin >> number;
        numberlist.push_back(number);
    }

    std::cout << "Thank you! Calculating average..."
        << std::endl;

    int total = 0;
    int average;
    for (int count = 0; count < N; count++) {
        total += numberlist[count];
        average = total / N;
    }

    std::cout << "The average of your numbers is:" << average
        << std::endl;

    return 0;
}