#include <iostream>

int Add(int a, int b) {

    int result = a + b;
    return result;
}

int main()
{
    int variable1;
    int variable2;

    std::cout << "Please enter number 1:" << std::endl;
    std::cin >> variable1;
    std::cout << "Please enter number 2:" << std::endl;
    std::cin >> variable2;

    int total = Add(variable1, variable2);
    std::cout << total;

    return 0;
}
