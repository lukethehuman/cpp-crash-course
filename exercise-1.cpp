#include <iostream>

int Add(int a, int b) {

    int result = a + b;
    return result;
}

int Subtract(int a, int b) {

    int result = a - b;
    return result;
}

int main()
{
    int variable1;
    int variable2;
    int variable3;

    std::cout << "Please enter a starting number:" << std::endl;
    std::cin >> variable1;
    std::cout << "Please enter a number to add:" << std::endl;
    std::cin >> variable2;
    std::cout << "Please enter a number to subtract:" << std::endl;
    std::cin >> variable3;

    int dummy = Add(variable1, variable2);
    int total = Subtract(dummy, variable3);
    std::cout << total;

    return 0;
}
